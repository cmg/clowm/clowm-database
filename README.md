# 🚨 Deprecation 🚨
This repository was merged into the [CloWM backend](https://gitlab.ub.uni-bielefeld.de/cmg/clowm/clowm-backend) and will NOT be developed further.  
Everything is in read-only mode.

# CloWM Database

## Description

This repository contains the database models and database session maker for the CloWM service.
MYSQL and MariaDB are supported.

## Usage

```python
from clowmdb.db.session import get_async_session, get_session
from clowmdb.models import Bucket
from sqlalchemy import select


async def database_usage_async(url: str):
    async with get_async_session(url=url) as db:
        stmt = select(Bucket)
        result = await db.execute(stmt)
        print(result)


def database_usage(url: str):
    with get_session(url=url) as db:
        stmt = select(Bucket)
        result = db.execute(stmt)
        print(result)
```

## Include it in a service

To include the database models into a service, simple add the package of this repository and the private GitLab
registry to the `requirements.txt`.

```
--extra-index-url https://gitlab.ub.uni-bielefeld.de/api/v4/projects/5493/packages/pypi/simple
clowmdb==<version>
```

To access the registry you need an access token with `read_api` scope. You can find further information
[here](https://docs.gitlab.com/ee/user/packages/pypi_repository/#using-requirementstxt).

For developing purposes you can download the repository and install it locally with `pip`. Navigate into the repository
and execute the following command

```shell
python setup.py install
```

To set up the database for testing in CI or locally, use the
[docker image of the repository](https://gitlab.ub.uni-bielefeld.de/groups/cmg/clowm/-/container_registries/242).
It will automatically upgrade the database schema to the latest version. Don't forget to set the right
[environment variables](#env-variables).

## Alter Database

### Upgrade database to latest version

```shell
alembic upgrade head
```

Or use the provided [Dockerfile](Dockerfile). Make sure to set the environment variables in the container and connect
it to the same network as the database.

### Downgrade database

```shell
alembic downgrade <revision id>
```

## Env variables

If you want to upgrade or downgrade the database schema, you have to set these environment variables

| Variable      | Default | Value              | Description                        |
|---------------|---------|--------------------|------------------------------------|
| `DB_HOST`     | unset   | <db hostname / IP> | IP Address or Hostname of database |
| `DB_PORT`     | 3306    | Number             | Port of the database               |
| `DB_USER`     | unset   | \<db username>     | Username of the database user      |
| `DB_PASSWORD` | unset   | \<db password>     | Password of the database user      |
| `DB_DATABASE` | unset   | \<db name>         | Name of the database               |

## License

The API is licensed under the [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) license.
See the [License](LICENSE) file for more information.
