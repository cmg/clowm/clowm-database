#!/usr/bin/env bash

set -x

ruff --version
ruff check clowmdb migrations setup.py
ruff format --diff clowmdb migrations setup.py

isort --version
isort -c clowmdb migrations setup.py

mypy --version
mypy clowmdb
