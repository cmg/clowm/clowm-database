#!/bin/sh -e
set -x

# Sort imports one per line, so autoflake can remove unused imports
ruff --fix --show-fixes clowmdb migrations
ruff format clowmdb migrations
isort clowmdb migrations
