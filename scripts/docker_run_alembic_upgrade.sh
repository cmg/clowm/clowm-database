#!/bin/sh -e
set -x

python check_database_connection.py
alembic upgrade head
