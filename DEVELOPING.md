# Development Setup

## Python Setup 🐍
Currently, only Python version `>=3.10` is supported because it uses its new features for type annotations
to write more compact code. Since FastAPI relies on these type annotations and `mypy` is integrated into
this project, we make heavy use of this feature.

Write
```python
var1: list[int] = [1,2,3]
var2: str | None = None
```
instead of
```python
from typing import List, Optional

var1: List[int] = [1,2,3]
var2: Optional[str] = None
```
## Environment Setup
Create a virtual environment, install the dependencies and install the [pre-commit](https://pre-commit.com/) hooks.<br>
The linters can prevent a commit if the code quality doesn't meet the standard.
```shell
python -m venv venv
source venv/bin/activate
python -m pip install -r requirements.txt
python -m pip install -r requirements-dev.txt
pre-commit install
```

You can check your code with linters or even automatically reformat files based on these rules
```shell
./scripts/lint.sh      # check code
./scripts/format.sh    # reformat code
```


## Change database schema
### Create a new version of the database schemas
```shell
alembic revision --autogenerate -m "<short description>"
```

### Alter existing table definition
Simply modify the appropriate classes and [create a new version](#create-a-new-version-of-the-database-schemas)
with `alembic`

### Create a new table
To create a new model, simply create a new file in the `models` folder. Then copy and modify this template into the
created file.
```python
from clowmdb.db.base_class import Base, CreateUpdateMixin

class ExampleModel(Base, CreateUpdateMixin):
    """
    Short Description of example model.
    """

    __tablename__: str = "example"
    ...
```
Afterwards, import the class in the file `db/base.py` to make it accessible for alembic.
```python
from models.example import ExampleModel  # noqa
```
Then [create a new version](#create-a-new-version-of-the-database-schemas) with `alembic`

### Publish the changes in the registry
The CI pipeline should automatically publish a new version in the registry, if you follow these simple steps
1. Make the changes
2. Increase the version number in [`clowmdb/__init__.py`](clowmdb/__init__.py) according to [semantic versioning](https://semver.org/)
3. Update the latest revision in [`clowmdb/__init__.py`](clowmdb/__init__.py) if necessary
4. Document the changes in [`CHANGELOG.md`](CHANGELOG.md)
5. Merge the changes into the `main` branch
6. Create a git tag on the main branch with the version, e.g. `v1.0.2`
