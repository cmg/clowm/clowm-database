# 3.2.1

* Make `lifescience_id` of `User` model optional with python type hints
* Use ruff instead of black for formatting

# 3.2.0

* Drop support for python 3.10

## Revision `68d3a8de7eba`
* Add columns `size_limit` and `object_limit` to `Bucket`
* Drop colum `owner_constraint` from `Bucket`
* Add `Role` and `UserRoleMapping` table

# 3.1.1

* Add `property` annotation to `ResourceVersion.synchronization_request_uid`

# 3.1.0

* Renamed attributes on models with type `bytes` from `_uid` to `uid_bytes`
* Drop support for Python 3.8 and 3.9

## Revision `a2194d0723b8`
* Add columns `synchronization_request_uid`, `synchronization_request_description`, `last_used_timestamp', `times_used` and `synchronization_request_uid` to `ResourceVersion`
* Add column `private` to `Resource`
* Add column `parameter_extension` to `WorkflowVersion`
* Add `WAIT_FOR_REVIEW` & `APPROVED` and remove `CLUSTER_DELETED` from `ResourceVersion.Status`

# 3.0.1

* Fix broken primary key of `BucketPermission` after migration

# 3.0.0

* Go from LifeScience ID to internal ID as primary Key for users
* Add several columns to store more information
* Upgrade Docker container to 3.12

## Revision `1bde8ed4ca83`

* Add
  columns `sync_finished_timestamp`, `sync_slurm_job_id`, `cluster_deleted_timestamp`, `cluster_delete_slurm_job_id`, `s3_deleted_timestamp`, `set_latest_timestamp`, `set_latest_slurm_job_id`
  and `remove_latest_timestamp` to `ResourceVersion`
* Add `SYNC_ERROR`, `SETTING_LATEST`, `CLUSTER_DELETING`, `CLUSTER_DELETE_ERROR`, `S3_DELETING` and `S3_DELETE_ERROR` to `ResourceVersion.Status`
* Change column name from `user_id` to `uid` in `BucketPermission`
* Change `uid` to UUID and add `lifescience_id`, `email` and `aruna_token` to `User`
* Change `user_id` to `executor_id` and add `cpu_hours` to `WorkflowExecution`

# 2.3.1

* Add `CLUSTER_DELETED` and `S3_DELETED` to `ResourceVersion.Status`

# 2.3.0

* Update linter dependencies

## Revision `410efa47f94c`

* Add tables `Resource` and `ResourceVersion`
* Remove unused colum `credentials_username` from `Workflow`
* Add `logs_path`, `debug_path` and `provenence_path` to `WorkflowExecution`

# 2.2.1

* Give the `onupdate` parameter for the `updated_at` column a lambda function instead of a fixed value
* Update dependencies

# 2.2.0

* Use `ruff` as linter instead of `flake8`
* Set `ON DELETE` clause on `workflow_mode_association_table`

## Revision `e161adcdd651`

* Add optional column `mode_id` to `WorkflowExecution`

# 2.1.0

* Changed all timestamps to integers which contain a UNIX timestamp

# 2.0.2

* Fix wrong Python type for model attribute `Workflow.versions`

# 2.0.1

* Add missing import of `WorkflowMode`
* Add tests to catch such errors earlier
* Upgrade Docker container to 3.11

# 2.0.0

* Upgrade SQLAlchemy to >= 2.0.0
* Drop Python 3.7 support
* Transform the functions to create a DB session into Context Managers

## Revision `61070219989b`

* Add credentials to `Workflow` model
* Add new model `WorkflowMode`

# 1.3.4

* Support docker secrets in database connection check

# 1.3.3

* Allow the Python type of `WorkflowExecution.workflow_version_id` to be `None`
* Add functions `active_workflows` and `inactive_workflows` to `WorkflowExecution.WorkflowExecutionStatus` to return a
  list of possible status for active/inactive workflows

# 1.3.2

* Enforce ForeignKey constraint if a user is deleted
    * User deleted -> Delete all Bucket Permissions of that user
    * User deleted -> Delete all Workflow Executions of that user
    * User deleted -> Set Developer ID of Workflow to NULL
* Make Python type of `WorkflowExecution.endtime` nullable like in the database
* Rename `BucketPermission.PermissionEnum` to `BucketPermission.Permission`
* Rename `Bucket.Constrain` to `Bucket.Constraint`
* Rename `WorkflowExecution.WorkflowExecutionStatusEnum` to `WorkflowExecution.WorkflowExecutionStatus`

# 1.3.1

* Set `owner_id` to Null if he was deleted on `Bucket`

# 1.3.0

* Set `workflow_version` to null of a `WorkflowExecution` if the version was deleted

## Revision `b88e083455d3`

* Add `slurm_job_id` to `WorkflowExecution`
* Add `owner_constrain` to `Bucket`

# 1.2.1

* Set previous commit hash to Null if it was deleted on `WorkflowVersion`

# 1.2.0

## Revision `18aee1a5d170`

* Add unified status column to `WorkflowVersion`
* Drop column `published` and `deprecated` of `WorkflowVersion`

# 1.1.2

* Fix default primary key for Workflow and WorkflowExecution
* Update dependencies
* Support docker secrets, e.g. search automatically for env variables with a `_FILE` suffix and try to read the
  contents of the file that this env variable is referring to

# 1.1.1

* Fix error in Docker container that entrypoint is not executable

# 1.1.0

* Publish Docker container automatically
  in [GitLab Container Registry](https://gitlab.ub.uni-bielefeld.de/groups/cmg/clowm/-/container_registries/242)
* Add variable `latest_revision` to `clowmdb` to get the latest database revision from the package
* Fix default port in `alembic` to `3306`
* Update documentation

# 1.0.0

## Revision `47d1ffbbfc92`

* Add Workflow model
* Add WorkflowVersion model
* Add WorkflowExecution model

## Revision `d557b4b44b7c`

* Add `created_at` column to each model with current timestamp as default value
* Add `updated_at` column to each model which will be changed on each update to the row with the current timestamp

## Revision `cafa1e01b782`

* Add bucket permission model

## Revision `5521b5759004`

* Add user model
* Add bucket model
