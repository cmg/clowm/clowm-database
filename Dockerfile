FROM python:3.12-slim

RUN useradd -m worker
USER worker
WORKDIR /home/worker/code
ENV PYTHONPATH=/home/worker/code
ENV PATH="/home/worker/.local/bin:${PATH}"

COPY --chown=worker:worker requirements.txt ./requirements.txt

RUN pip install --disable-pip-version-check --user --no-cache-dir --upgrade -r requirements.txt

COPY --chown=worker:worker . .

CMD ["./scripts/docker_run_alembic_upgrade.sh"]
