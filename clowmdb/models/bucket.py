from typing import TYPE_CHECKING, Any
from uuid import UUID

from sqlalchemy import Boolean, ForeignKey, String
from sqlalchemy.dialects.mysql import INTEGER, TEXT
from sqlalchemy.orm import Mapped, mapped_column, relationship

from clowmdb.db.base_class import Base

if TYPE_CHECKING:
    from .bucket_permission import BucketPermission
    from .user import User


class Bucket(Base):
    """
    Database model for a bucket.
    """

    __tablename__: str = "bucket"
    name: Mapped[str] = mapped_column(String(63), primary_key=True, index=True, unique=True)
    description: Mapped[str] = mapped_column(TEXT, nullable=False)
    public: Mapped[bool] = mapped_column(Boolean(), default=False, server_default="0", nullable=False)
    owner_id_bytes: Mapped[bytes | None] = mapped_column(
        ForeignKey("user.uid", ondelete="SET NULL"), name="owner_id", nullable=True
    )
    owner: Mapped["User"] = relationship("User", back_populates="buckets")
    permissions: Mapped[list["BucketPermission"]] = relationship(
        "BucketPermission",
        back_populates="bucket",
        cascade="all, delete",
        passive_deletes=True,
    )
    # size limit of bucket in KiB
    size_limit: Mapped[int | None] = mapped_column(INTEGER(unsigned=True), nullable=True)
    object_limit: Mapped[int | None] = mapped_column(INTEGER(unsigned=True), nullable=True)
    __mapper_args__ = {"eager_defaults": True}

    @property
    def owner_id(self) -> UUID | None:
        return UUID(bytes=self.owner_id_bytes) if self.owner_id_bytes is not None else None

    def __eq__(self, other: Any) -> bool:
        return self.name == other.name if isinstance(other, Bucket) else False

    def __repr__(self) -> str:
        return f"'Bucket(name={self.name}', owner='{self.owner_id}')"
