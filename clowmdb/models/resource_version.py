from enum import StrEnum, unique
from typing import TYPE_CHECKING, Any
from uuid import UUID

from sqlalchemy import BINARY, ForeignKey, Integer, String, text
from sqlalchemy.dialects.mysql import ENUM
from sqlalchemy.orm import Mapped, mapped_column, relationship

from clowmdb.db.base_class import Base, uuid7

if TYPE_CHECKING:
    from .resource import Resource


class ResourceVersion(Base):
    """
    Database model for a Resource Version.
    """

    @unique
    class Status(StrEnum):
        """
        Enumeration for the possible status of a resource version.
        """

        RESOURCE_REQUESTED = "RESOURCE_REQUESTED"
        WAIT_FOR_REVIEW = "WAIT_FOR_REVIEW"
        DENIED = "DENIED"
        APPROVED = "APPROVED"
        SYNC_REQUESTED = "SYNC_REQUESTED"
        SYNCHRONIZING = "SYNCHRONIZING"
        SYNC_ERROR = "SYNC_ERROR"
        SYNCHRONIZED = "SYNCHRONIZED"
        SETTING_LATEST = "SETTING_LATEST"
        LATEST = "LATEST"
        CLUSTER_DELETING = "CLUSTER_DELETING"
        CLUSTER_DELETE_ERROR = "CLUSTER_DELETE_ERROR"
        S3_DELETING = "S3_DELETING"
        S3_DELETE_ERROR = "S3_DELETE_ERROR"
        S3_DELETED = "S3_DELETED"

        @property
        def public(self) -> bool:
            return self.name not in {
                ResourceVersion.Status.RESOURCE_REQUESTED.name,
                ResourceVersion.Status.WAIT_FOR_REVIEW.name,
                ResourceVersion.Status.DENIED.name,
                ResourceVersion.Status.S3_DELETING.name,
                ResourceVersion.Status.S3_DELETE_ERROR.name,
                ResourceVersion.Status.S3_DELETED.name,
            }

    __tablename__ = "resource_version"
    resource_version_id_bytes: Mapped[bytes] = mapped_column(
        BINARY(16),
        name="resource_version_id",
        primary_key=True,
        index=True,
        unique=True,
        default=lambda: uuid7().bytes,
    )
    release: Mapped[str] = mapped_column(String(32), nullable=False)
    status: Mapped[str | Status] = mapped_column(ENUM(Status), default=Status.RESOURCE_REQUESTED, nullable=False)
    sync_finished_timestamp: Mapped[int | None] = mapped_column(Integer, nullable=True)
    sync_slurm_job_id: Mapped[int] = mapped_column(Integer, nullable=True)
    cluster_deleted_timestamp: Mapped[int | None] = mapped_column(Integer, nullable=True)
    cluster_delete_slurm_job_id: Mapped[int] = mapped_column(Integer, nullable=True)
    s3_deleted_timestamp: Mapped[int | None] = mapped_column(Integer, nullable=True)
    set_latest_timestamp: Mapped[int | None] = mapped_column(Integer, nullable=True)
    set_latest_slurm_job_id: Mapped[int | None] = mapped_column(Integer, nullable=True)
    remove_latest_timestamp: Mapped[int | None] = mapped_column(Integer, nullable=True)
    resource_id_bytes: Mapped[bytes] = mapped_column(
        ForeignKey("resource.resource_id", ondelete="CASCADE"), name="resource_id", nullable=False
    )
    synchronization_request_uid_bytes: Mapped[bytes | None] = mapped_column(
        ForeignKey("user.uid", ondelete="SET NULL"), name="synchronization_request_uid", nullable=True
    )
    synchronization_request_description: Mapped[str | None] = mapped_column(String(512), nullable=True)
    last_used_timestamp: Mapped[int | None] = mapped_column(Integer, nullable=True)
    times_used: Mapped[int] = mapped_column(Integer, nullable=False, default=0, server_default=text("0"))
    resource: Mapped["Resource"] = relationship("Resource", back_populates="versions")
    previous_resource_version_id_bytes: Mapped[bytes | None] = mapped_column(
        ForeignKey("resource_version.resource_version_id", ondelete="SET NULL"),
        name="previous_resource_version_id",
        nullable=True,
    )

    @property
    def resource_version_id(self) -> UUID:
        return UUID(bytes=self.resource_version_id_bytes)

    @property
    def previous_resource_version_id(self) -> UUID | None:
        return (
            UUID(bytes=self.previous_resource_version_id_bytes)
            if self.previous_resource_version_id_bytes is not None
            else None
        )

    @property
    def synchronization_request_uid(self) -> UUID | None:
        return (
            UUID(bytes=self.synchronization_request_uid_bytes)
            if self.synchronization_request_uid_bytes is not None
            else None
        )

    @property
    def resource_id(self) -> UUID:
        return UUID(bytes=self.resource_id_bytes)

    @resource_id.setter
    def resource_id(self, value: UUID) -> None:
        assert isinstance(value, UUID)
        self.resource_id_bytes = value.bytes

    __mapper_args__ = {"eager_defaults": True}

    def __eq__(self, other: Any) -> bool:
        return self.resource_version_id == other.resource_version_id if isinstance(other, ResourceVersion) else False

    def __repr__(self) -> str:
        return f"'ResourceVersion(id={self.resource_version_id}', release='{self.release}', status='{self.status}', resource='{self.resource_id}')"
