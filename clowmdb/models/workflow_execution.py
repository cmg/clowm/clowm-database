from enum import StrEnum, unique
from typing import TYPE_CHECKING, Any
from uuid import UUID

from sqlalchemy import BINARY, Float, ForeignKey, Integer, String, text
from sqlalchemy.dialects.mysql import ENUM, TEXT
from sqlalchemy.orm import Mapped, mapped_column, relationship

from clowmdb.db.base_class import Base, uuid7

if TYPE_CHECKING:
    from .user import User
    from .workflow_version import WorkflowVersion


class WorkflowExecution(Base):
    """
    Database model for an execution of a workflow.
    """

    @unique
    class WorkflowExecutionStatus(StrEnum):
        """
        Enumeration for the status on a workflow execution.
        """

        PENDING = "PENDING"
        SCHEDULED = "SCHEDULED"
        RUNNING = "RUNNING"
        CANCELED = "CANCELED"
        SUCCESS = "SUCCESS"
        ERROR = "ERROR"

        @staticmethod
        def active_workflows() -> list["WorkflowExecution.WorkflowExecutionStatus"]:
            return [
                WorkflowExecution.WorkflowExecutionStatus.PENDING,
                WorkflowExecution.WorkflowExecutionStatus.SCHEDULED,
                WorkflowExecution.WorkflowExecutionStatus.RUNNING,
            ]

        @staticmethod
        def inactive_workflows() -> list["WorkflowExecution.WorkflowExecutionStatus"]:
            return [
                WorkflowExecution.WorkflowExecutionStatus.CANCELED,
                WorkflowExecution.WorkflowExecutionStatus.SUCCESS,
                WorkflowExecution.WorkflowExecutionStatus.ERROR,
            ]

    __tablename__: str = "workflow_execution"

    execution_id_bytes: Mapped[bytes] = mapped_column(
        BINARY(16),
        name="execution_id",
        primary_key=True,
        index=True,
        unique=True,
        default=lambda: uuid7().bytes,
    )
    executor_id_bytes: Mapped[bytes] = mapped_column(
        ForeignKey("user.uid", ondelete="CASCADE"), name="executor_id", nullable=False
    )
    workflow_version_id: Mapped[str | None] = mapped_column(
        ForeignKey("workflow_version.git_commit_hash", ondelete="SET NULL"), nullable=True
    )
    _workflow_mode_id: Mapped[bytes | None] = mapped_column(
        ForeignKey("workflow_mode.mode_id", ondelete="SET NULL"), name="workflow_mode_id", nullable=True
    )
    debug_path: Mapped[str | None] = mapped_column(String(1024), nullable=True)
    logs_path: Mapped[str | None] = mapped_column(String(1024), nullable=True)
    provenance_path: Mapped[str | None] = mapped_column(String(1024), nullable=True)
    start_time: Mapped[int] = mapped_column(Integer, nullable=False, server_default=text("UNIX_TIMESTAMP()"))
    end_time: Mapped[int | None] = mapped_column(Integer, nullable=True)
    cpu_hours: Mapped[float] = mapped_column(Float, default=0.0, nullable=False, server_default=text("0"))
    status: Mapped[str | WorkflowExecutionStatus] = mapped_column(
        ENUM(WorkflowExecutionStatus), default=WorkflowExecutionStatus.PENDING, nullable=False
    )
    notes: Mapped[str | None] = mapped_column(TEXT, nullable=True)
    slurm_job_id: Mapped[int] = mapped_column(Integer, nullable=False)
    user: Mapped["User"] = relationship("User", back_populates="workflow_executions")
    workflow_version: Mapped["WorkflowVersion"] = relationship("WorkflowVersion", back_populates="workflow_executions")

    @property
    def execution_id(self) -> UUID:
        return UUID(bytes=self.execution_id_bytes)

    @property
    def workflow_mode_id(self) -> UUID | None:
        return UUID(bytes=self._workflow_mode_id) if self._workflow_mode_id is not None else None

    @property
    def executor_id(self) -> UUID | None:
        return UUID(bytes=self.executor_id_bytes) if self.executor_id_bytes is not None else None

    def __repr__(self) -> str:
        return "WorkflowExecution(execution_id={} user={} workflow_version={})".format(
            self.execution_id, self.executor_id, self.workflow_version_id
        )

    def __eq__(self, other: Any) -> bool:
        return self.execution_id == other.execution_id if isinstance(other, WorkflowExecution) else False
