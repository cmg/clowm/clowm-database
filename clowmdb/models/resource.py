from typing import TYPE_CHECKING, Any
from uuid import UUID

from sqlalchemy import BINARY, Boolean, ForeignKey, String, text
from sqlalchemy.orm import Mapped, mapped_column, relationship

from clowmdb.db.base_class import Base, uuid7

if TYPE_CHECKING:
    from .resource_version import ResourceVersion
    from .user import User


class Resource(Base):
    """
    Database model for a Resource.
    """

    __tablename__: str = "resource"
    resource_id_bytes: Mapped[bytes] = mapped_column(
        BINARY(16),
        name="resource_id",
        primary_key=True,
        index=True,
        unique=True,
        default=lambda: uuid7().bytes,
    )
    name: Mapped[str] = mapped_column(String(32), unique=True, nullable=False)
    short_description: Mapped[str] = mapped_column(String(264), nullable=False)
    source: Mapped[str] = mapped_column(String(264))
    private: Mapped[bool] = mapped_column(Boolean, default=True, server_default=text("1"))
    maintainer_id_bytes: Mapped[bytes | None] = mapped_column(
        ForeignKey("user.uid", ondelete="SET NULL"), name="maintainer_id", nullable=True
    )
    maintainer: Mapped["User"] = relationship("User", back_populates="resources")
    versions: Mapped[list["ResourceVersion"]] = relationship(
        "ResourceVersion", back_populates="resource", cascade="all, delete", passive_deletes=True
    )

    __mapper_args__ = {"eager_defaults": True}

    @property
    def resource_id(self) -> UUID:
        return UUID(bytes=self.resource_id_bytes)

    @property
    def maintainer_id(self) -> UUID | None:
        return UUID(bytes=self.maintainer_id_bytes) if self.maintainer_id_bytes is not None else None

    def __eq__(self, other: Any) -> bool:
        return self.resource_id == other.resource_id if isinstance(other, Resource) else False

    def __repr__(self) -> str:
        return f"'Resource(id={self.resource_id}', name='{self.name}')"
