from typing import Any
from uuid import UUID

from sqlalchemy import BINARY, String
from sqlalchemy.orm import Mapped, mapped_column

from clowmdb.db.base_class import Base, uuid7


class WorkflowMode(Base):
    __tablename__: str = "workflow_mode"
    mode_id_bytes: Mapped[bytes] = mapped_column(
        BINARY(16),
        name="mode_id",
        primary_key=True,
        index=True,
        unique=True,
        default=lambda: uuid7().bytes,
    )
    schema_path: Mapped[str] = mapped_column(String(256))
    entrypoint: Mapped[str] = mapped_column(String(256))
    name: Mapped[str] = mapped_column(String(128))

    __mapper_args__ = {"eager_defaults": True}

    @property
    def mode_id(self) -> UUID:
        return UUID(bytes=self.mode_id_bytes)

    def __eq__(self, other: Any) -> bool:
        return self.mode_id == other.mode_id if isinstance(other, WorkflowMode) else False

    def __repr__(self) -> str:
        return f"'WorkflowModus(id={self.mode_id}', name='{self.name}, entrypoint='{self.entrypoint}')"
