from enum import StrEnum, unique
from typing import TYPE_CHECKING
from uuid import UUID

from sqlalchemy import ForeignKey, Integer, String
from sqlalchemy.dialects.mysql import ENUM
from sqlalchemy.orm import Mapped, mapped_column, relationship

from clowmdb.db.base_class import Base

if TYPE_CHECKING:
    from .bucket import Bucket
    from .user import User


class BucketPermission(Base):
    @unique
    class Permission(StrEnum):
        """
        Enumeration for the possible permission on a bucket.
        """

        READ = "READ"
        WRITE = "WRITE"
        READWRITE = "READWRITE"

    """
    Database model for the permission for a user on a bucket.
    Will be deleted if either the user or the bucket is deleted.
    """

    __tablename__: str = "bucketpermission"
    uid_bytes: Mapped[bytes] = mapped_column(ForeignKey("user.uid", ondelete="CASCADE"), name="uid", primary_key=True)
    bucket_name: Mapped[str] = mapped_column(ForeignKey("bucket.name", ondelete="CASCADE"), primary_key=True)
    from_: Mapped[int | None] = mapped_column("from", Integer, nullable=True)
    to: Mapped[int | None] = mapped_column(Integer, nullable=True)
    file_prefix: Mapped[str | None] = mapped_column(String(512), nullable=True)
    permissions: Mapped[str | Permission] = mapped_column(ENUM(Permission), default=Permission.READ, nullable=False)
    grantee: Mapped["User"] = relationship("User", back_populates="permissions")
    bucket: Mapped["Bucket"] = relationship("Bucket", back_populates="permissions")

    @property
    def uid(self) -> UUID:
        return UUID(bytes=self.uid_bytes)

    def __repr__(self) -> str:
        return f"BucketPermission(uid={self.uid} bucket_name={self.bucket_name})"
