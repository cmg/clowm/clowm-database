from typing import TYPE_CHECKING, Any
from uuid import UUID

from sqlalchemy import BINARY, Boolean, String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from clowmdb.db.base_class import Base, uuid7

from .role import Role, RoleIdMapping, UserRoleMapping

if TYPE_CHECKING:
    from .bucket import Bucket
    from .bucket_permission import BucketPermission
    from .resource import Resource
    from .workflow import Workflow
    from .workflow_execution import WorkflowExecution


class User(Base):
    """
    Database model for a user.
    """

    __tablename__: str = "user"
    uid_bytes: Mapped[bytes] = mapped_column(
        BINARY(16),
        name="uid",
        primary_key=True,
        index=True,
        unique=True,
        default=lambda: uuid7().bytes,
    )
    lifescience_id: Mapped[str | None] = mapped_column(String(64), index=True, unique=True, nullable=True)
    display_name: Mapped[str] = mapped_column(String(256), nullable=False)
    email: Mapped[str | None] = mapped_column(String(256), nullable=True)
    aruna_token: Mapped[str | None] = mapped_column(String(128), nullable=True)
    buckets: Mapped[list["Bucket"]] = relationship("Bucket", back_populates="owner")
    permissions: Mapped[list["BucketPermission"]] = relationship(
        "BucketPermission",
        back_populates="grantee",
        cascade="all, delete",
        passive_deletes=True,
    )
    workflows: Mapped[list["Workflow"]] = relationship("Workflow", back_populates="developer")
    workflow_executions: Mapped[list["WorkflowExecution"]] = relationship(
        "WorkflowExecution", back_populates="user", cascade="all, delete", passive_deletes=True
    )
    resources: Mapped[list["Resource"]] = relationship("Resource", back_populates="maintainer", passive_deletes=False)
    initialized: Mapped[bool] = mapped_column(Boolean, default=False, nullable=False)
    roles: Mapped[list["UserRoleMapping"]] = relationship(
        back_populates="user", cascade="all, delete", passive_deletes=True
    )

    def has_role(self, role: Role.RoleEnum) -> bool:
        mapping = RoleIdMapping()
        return mapping[role] in (db_role.role_id for db_role in self.roles)

    @property
    def uid(self) -> UUID:
        return UUID(bytes=self.uid_bytes)

    def __eq__(self, other: Any) -> bool:
        return self.uid == other.uid if isinstance(other, User) else False

    def __repr__(self) -> str:
        return f"'User(uid={self.uid}', display_name='{self.display_name}')"
