from typing import TYPE_CHECKING, Any
from uuid import UUID

from sqlalchemy import BINARY, ForeignKey, String
from sqlalchemy.dialects.mysql import TEXT
from sqlalchemy.orm import Mapped, mapped_column, relationship

from clowmdb.db.base_class import Base, uuid7

if TYPE_CHECKING:
    from .user import User
    from .workflow_version import WorkflowVersion


class Workflow(Base):
    """
    Database model for a Workflow.
    """

    __tablename__: str = "workflow"
    workflow_id_bytes: Mapped[bytes] = mapped_column(
        BINARY(16),
        name="workflow_id",
        primary_key=True,
        index=True,
        unique=True,
        default=lambda: uuid7().bytes,
    )
    name: Mapped[str] = mapped_column(String(63), unique=True, nullable=False)
    repository_url: Mapped[str] = mapped_column(TEXT, nullable=False)
    short_description: Mapped[str] = mapped_column(TEXT, nullable=False)
    credentials_token: Mapped[str | None] = mapped_column(String(128))
    developer_id_bytes: Mapped[bytes | None] = mapped_column(
        ForeignKey("user.uid", ondelete="SET NULL"), name="developer_id", nullable=True
    )
    developer: Mapped["User"] = relationship("User", back_populates="workflows")
    versions: Mapped[list["WorkflowVersion"]] = relationship(
        "WorkflowVersion", back_populates="workflow", cascade="all, delete", passive_deletes=True
    )

    __mapper_args__ = {"eager_defaults": True}

    @property
    def workflow_id(self) -> UUID:
        return UUID(bytes=self.workflow_id_bytes)

    @property
    def developer_id(self) -> UUID | None:
        return UUID(bytes=self.developer_id_bytes) if self.developer_id_bytes is not None else None

    def __eq__(self, other: Any) -> bool:
        return self.workflow_id == other.workflow_id if isinstance(other, Workflow) else False

    def __repr__(self) -> str:
        return f"'Workflow(id={self.workflow_id}', name='{self.name}')"

    @property
    def private_repository(self) -> bool:
        return self.credentials_token is not None
