from enum import StrEnum, unique
from typing import TYPE_CHECKING, Any, Final
from uuid import UUID

from sqlalchemy import JSON, Column, ForeignKey, String, Table
from sqlalchemy.dialects.mysql import ENUM
from sqlalchemy.orm import Mapped, backref, mapped_column, relationship

from clowmdb.db.base_class import Base

if TYPE_CHECKING:
    from .workflow import Workflow
    from .workflow_execution import WorkflowExecution
    from .workflow_mode import WorkflowMode

workflow_mode_association_table: Final[Table] = Table(
    "workflow_modus_to_version_mapping",
    Base.metadata,
    Column(
        "workflow_version_commit_hash",
        ForeignKey("workflow_version.git_commit_hash", ondelete="CASCADE"),
        primary_key=True,
    ),
    Column("workflow_mode_id", ForeignKey("workflow_mode.mode_id", ondelete="CASCADE"), primary_key=True),
)


class WorkflowVersion(Base):
    """
    Database model for a Workflow Version.
    """

    @unique
    class Status(StrEnum):
        """
        Enumeration for the possible status of a workflow version.
        """

        CREATED = "CREATED"
        DENIED = "DENIED"
        PUBLISHED = "PUBLISHED"
        DEPRECATED = "DEPRECATED"

    __tablename__: str = "workflow_version"
    git_commit_hash: Mapped[str] = mapped_column(String(40), primary_key=True, index=True, unique=True)
    version: Mapped[str] = mapped_column(String(10), nullable=False)
    status: Mapped[str | Status] = mapped_column(ENUM(Status), default=Status.CREATED, nullable=False)
    icon_slug: Mapped[str | None] = mapped_column(String(48), nullable=True)
    parameter_extension: Mapped[dict | None] = mapped_column(JSON, nullable=True)
    workflow_id_bytes: Mapped[bytes] = mapped_column(
        ForeignKey("workflow.workflow_id", ondelete="CASCADE"), name="workflow_id", nullable=False
    )
    workflow: Mapped["Workflow"] = relationship("Workflow", back_populates="versions")
    previous_version_hash: Mapped[str | None] = mapped_column(
        ForeignKey("workflow_version.git_commit_hash", ondelete="SET NULL"), nullable=True
    )
    previous_version: Mapped["WorkflowVersion"] = relationship(
        "WorkflowVersion", backref=backref("next_version", uselist=False), remote_side="WorkflowVersion.git_commit_hash"
    )
    workflow_executions: Mapped[list["WorkflowExecution"]] = relationship(
        "WorkflowExecution", back_populates="workflow_version"
    )
    workflow_modes: Mapped[list["WorkflowMode"]] = relationship(secondary=workflow_mode_association_table)

    @property
    def workflow_id(self) -> UUID:
        return UUID(bytes=self.workflow_id_bytes)

    @workflow_id.setter
    def workflow_id(self, value: UUID) -> None:
        assert isinstance(value, UUID)
        self.workflow_id_bytes = value.bytes

    __mapper_args__ = {"eager_defaults": True}

    def __eq__(self, other: Any) -> bool:
        return self.git_commit_hash == other.git_commit_hash if isinstance(other, WorkflowVersion) else False

    def __repr__(self) -> str:
        return f"'WorkflowVersion(hash={self.git_commit_hash}', version='{self.version}', status='{self.status}')"
