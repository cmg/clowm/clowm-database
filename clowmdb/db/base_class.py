import struct
import time
from os import urandom
from uuid import UUID

from sqlalchemy import Integer, text
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column


def uuid7() -> UUID:
    # https://www.ietf.org/archive/id/draft-ietf-uuidrev-rfc4122bis-11.html#name-uuid-version-7
    time_ms = struct.pack(">Q", round(time.time() * 1000))[2:]
    ver_plus_a = (7 << 12) | (int.from_bytes(urandom(2)) >> 4)
    var_plus_b = (2 << 62) | (int.from_bytes(urandom(8)) >> 2)
    return UUID(bytes=struct.pack(">6sHQ", time_ms, ver_plus_a, var_plus_b))


class CreateUpdateMixin:
    created_at: Mapped[int] = mapped_column(Integer, nullable=False, server_default=text("(UNIX_TIMESTAMP())"))
    updated_at: Mapped[int] = mapped_column(
        Integer, nullable=False, server_default=text("(UNIX_TIMESTAMP())"), onupdate=lambda: round(time.time())
    )


class Base(DeclarativeBase, CreateUpdateMixin):
    pass
