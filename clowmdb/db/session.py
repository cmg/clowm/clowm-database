from contextlib import asynccontextmanager, contextmanager
from typing import AsyncIterator, Iterator

from sqlalchemy import create_engine
from sqlalchemy.ext.asyncio import AsyncSession, async_sessionmaker, create_async_engine
from sqlalchemy.orm import Session, sessionmaker


@contextmanager
def get_session(url: str, verbose: bool = False) -> Iterator[Session]:
    """
    Get a sessionmaker that can be used as a context manager

    Parameters
    ----------
    url : str
        The url for connecting to the database
    verbose : bool, default False
        Flag for more verbose output

    Returns
    -------
    sessionmaker : sqlalchemy.orm.sessionmaker
        The sessionmaker for creating a session

    Notes
    -----
    The sessionmaker can be used as a context manager:

    with get_session(url=db_url) as db:
            db.select(...)
    """
    engine = create_engine(url, pool_pre_ping=True, pool_recycle=3600, echo=verbose)
    with sessionmaker(autocommit=False, autoflush=False, bind=engine)() as db:
        yield db


@asynccontextmanager
async def get_async_session(url: str, verbose: bool = False) -> AsyncIterator[AsyncSession]:
    """
    Get a sessionmaker that can be used as an async context manager

    Parameters
    ----------
    url : str
        The url for connecting to the database
    verbose : bool, default False
        Flag for more verbose output

    Returns
    -------
    sessionmaker : sqlalchemy.orm.sessionmaker
        The sessionmaker for creating a async session

    Notes
    -----
    The sessionmaker can be used as an async context manager:

    async with get_async_session(url=db_url) as db:
            await db.select(...)
    """
    engineAsync = create_async_engine(url, echo=verbose, pool_recycle=3600)
    async with async_sessionmaker(engineAsync, expire_on_commit=False)() as session:
        yield session
    await engineAsync.dispose()
