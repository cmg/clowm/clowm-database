import random
import string

from sqlalchemy.sql import Executable

CleanupList = list[Executable]


def random_lower_string(length: int = 32) -> str:
    """
    Creates a random string with arbitrary length.

    Parameters
    ----------
    length : int, default 32
        Length for the random string.

    Returns
    -------
    string : str
        Random string.
    """
    return "".join(random.choices(string.ascii_lowercase, k=length))


def random_hex_string(length: int = 40) -> str:
    """
    Creates a random string with only hexadecimal digits

    Parameters
    ----------
    length : int, default 32
        Length for the random string.

    Returns
    -------
    string : str
        Random string.
    """
    return "".join(random.choices("0123456789abcdef", k=length))
