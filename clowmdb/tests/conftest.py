import asyncio
from typing import AsyncGenerator, Generator

import pytest
import pytest_asyncio
from sqlalchemy import insert
from sqlalchemy.ext.asyncio import AsyncSession

from clowmdb.db.session import get_async_session
from clowmdb.models import (
    Bucket,
    Resource,
    ResourceVersion,
    User,
    Workflow,
    WorkflowMode,
    WorkflowVersion,
    workflow_mode_association_table,
)
from clowmdb.models.role import RoleIdMapping
from clowmdb.tests.testutils import CleanupList, random_hex_string, random_lower_string
from envutil import get_url


@pytest.fixture(scope="session")
def event_loop() -> Generator:
    """
    Creates an instance of the default event loop for the test session.
    """
    loop = asyncio.new_event_loop()
    yield loop
    loop.close()


@pytest_asyncio.fixture(scope="module")
async def db() -> AsyncGenerator[AsyncSession, None]:
    """
    Fixture for creating a database session to connect to.
    """
    async with get_async_session(url=get_url(async_url=True), verbose=False) as dbSession:
        await RoleIdMapping().load_role_ids(db=dbSession)
        yield dbSession


@pytest_asyncio.fixture(scope="function")
async def random_user(db: AsyncSession) -> AsyncGenerator[User, None]:
    """
    Create a random user and deletes him afterward.
    """
    user = User(lifescience_id=random_hex_string(), display_name=random_lower_string(), email="test@example.org")
    db.add(user)
    await db.commit()
    yield user
    await db.delete(user)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_bucket(db: AsyncSession, random_user: User) -> AsyncGenerator[Bucket, None]:
    """
    Create a random bucket and deletes it afterward.
    """
    bucket = Bucket(name=random_lower_string(), description=random_lower_string(), owner_id_bytes=random_user.uid.bytes)
    db.add(bucket)
    await db.commit()
    yield bucket
    await db.delete(bucket)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_workflow(db: AsyncSession, random_user: User) -> AsyncGenerator[Workflow, None]:
    """
    Create a random workflow and deletes it afterward.
    """
    workflow = Workflow(
        name=random_lower_string(),
        repository_url=random_lower_string(),
        short_description=random_lower_string(),
        developer_id_bytes=random_user.uid.bytes,
    )
    db.add(workflow)
    await db.commit()
    await db.refresh(workflow)
    yield workflow
    await db.delete(workflow)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_workflow_version(db: AsyncSession, random_workflow: Workflow) -> AsyncGenerator[WorkflowVersion, None]:
    """
    Create a random workflow version and deletes it afterward.
    """
    version = WorkflowVersion(
        git_commit_hash=random_hex_string(),
        version=random_lower_string(5),
        workflow_id_bytes=random_workflow.workflow_id.bytes,
    )
    db.add(version)
    await db.commit()
    await db.refresh(version)
    yield version
    await db.delete(version)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_workflow_mode(
    db: AsyncSession, random_workflow_version: WorkflowVersion
) -> AsyncGenerator[WorkflowMode, None]:
    """
    Create a random workflow mode, attach it to the random workflow version and deletes it afterward.
    """
    mode = WorkflowMode(schema_path=random_lower_string(), entrypoint=random_lower_string(), name=random_lower_string())
    db.add(mode)
    await db.commit()
    await db.execute(
        insert(workflow_mode_association_table),
        {
            "workflow_version_commit_hash": random_workflow_version.git_commit_hash,
            "workflow_mode_id": mode.mode_id.bytes,
        },
    )
    await db.commit()
    yield mode
    await db.delete(mode)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_resource(db: AsyncSession, random_user: User) -> AsyncGenerator[Resource, None]:
    """
    Create a random resource, attach it to the random user and deletes it afterward.
    """
    resource = Resource(
        name=random_lower_string(16),
        short_description=random_lower_string(),
        source=random_lower_string(),
        maintainer_id_bytes=random_user.uid.bytes,
    )
    db.add(resource)
    await db.commit()
    yield resource
    await db.delete(resource)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_resource_version(db: AsyncSession, random_resource: Resource) -> AsyncGenerator[ResourceVersion, None]:
    """
    Create a random resource version, attach it to the random user and deletes it afterward.
    """
    resource_version = ResourceVersion(
        release=random_lower_string(16), resource_id_bytes=random_resource.resource_id.bytes
    )
    db.add(resource_version)
    await db.commit()
    yield resource_version
    await db.delete(resource_version)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def cleanup(db: AsyncSession) -> AsyncGenerator[CleanupList, None]:
    """
    Yield a list where SQL statements can be added tob e executed after a test is finished.
    """
    cleanup_list: CleanupList = []
    yield cleanup_list
    for stmt in cleanup_list:
        await db.execute(stmt)
    await db.commit()
