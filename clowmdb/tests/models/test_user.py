import pytest
from sqlalchemy import delete
from sqlalchemy.ext.asyncio import AsyncSession

from clowmdb.models import User
from clowmdb.tests.testutils import CleanupList, random_hex_string, random_lower_string


@pytest.mark.asyncio
async def test_user_insert(db: AsyncSession, cleanup: CleanupList) -> None:
    """
    Test for inserting a user successfully into the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    cleanup : list[sqlalchemy.sql.Executable]
        List where to add sql statements that get executed after the (failed) test.
    """
    user = User(lifescience_id=random_hex_string(), display_name=random_lower_string(), email="test@example.org")
    db.add(user)
    await db.commit()
    cleanup.append(delete(User).where(User.uid_bytes == user.uid_bytes))
    await db.refresh(user)
    assert user.uid is not None
