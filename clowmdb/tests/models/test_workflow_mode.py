import pytest
from sqlalchemy import delete, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload

from clowmdb.models import WorkflowMode, WorkflowVersion
from clowmdb.tests.testutils import CleanupList, random_lower_string


@pytest.mark.asyncio
async def test_workflow_mode_insert(
    db: AsyncSession, random_workflow_version: WorkflowVersion, cleanup: CleanupList
) -> None:
    """
    Test for inserting a workflow mode successfully into the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    random_workflow_version : clowmdb.models.WorkflowVersion
        Random workflow version in database.
    cleanup : list[sqlalchemy.sql.Executable]
        List where to add sql statements that get executed after the (failed) test.
    """
    mode = WorkflowMode(schema_path=random_lower_string(), entrypoint=random_lower_string(), name=random_lower_string())
    db.add(mode)
    version = (
        await db.execute(
            select(WorkflowVersion)
            .where(WorkflowVersion.git_commit_hash == random_workflow_version.git_commit_hash)
            .options(selectinload(WorkflowVersion.workflow_modes))
        )
    ).scalar()
    assert version is not None
    version.workflow_modes.append(mode)
    await db.commit()
    cleanup.append(delete(WorkflowMode).where(WorkflowMode.mode_id_bytes == mode.mode_id_bytes))
    assert mode.mode_id is not None


@pytest.mark.asyncio
async def test_workflow_mode_insert_and_delete(
    db: AsyncSession, random_workflow_version: WorkflowVersion, cleanup: CleanupList
) -> None:
    """
    Test for inserting a workflow mode successfully into the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    random_workflow_version : clowmdb.models.WorkflowVersion
        Random workflow version in database.
    cleanup : list[sqlalchemy.sql.Executable]
        List where to add sql statements that get executed after the (failed) test.
    """
    mode = WorkflowMode(schema_path=random_lower_string(), entrypoint=random_lower_string(), name=random_lower_string())
    db.add(mode)
    version = (
        await db.execute(
            select(WorkflowVersion)
            .where(WorkflowVersion.git_commit_hash == random_workflow_version.git_commit_hash)
            .options(selectinload(WorkflowVersion.workflow_modes))
        )
    ).scalar()
    assert version is not None
    version.workflow_modes.append(mode)
    await db.commit()
    cleanup.append(delete(WorkflowMode).where(WorkflowMode.mode_id_bytes == mode.mode_id_bytes))
    await db.delete(mode)
    await db.commit()
