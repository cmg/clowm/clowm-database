import pytest
from sqlalchemy import delete
from sqlalchemy.ext.asyncio import AsyncSession

from clowmdb.models import Resource, User
from clowmdb.tests.testutils import CleanupList, random_lower_string


@pytest.mark.asyncio(scope="session")
async def test_resource_insert(db: AsyncSession, random_user: User, cleanup: CleanupList) -> None:
    """
    Test for inserting a resource successfully into the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    random_user : clowmdb.models.User
        Random user in database.
    cleanup : list[sqlalchemy.sql.Executable]
        List where to add sql statements that get executed after the (failed) test.
    """
    resource = Resource(
        name=random_lower_string(16),
        short_description=random_lower_string(),
        source=random_lower_string(),
        maintainer_id_bytes=random_user.uid.bytes,
    )
    db.add(resource)
    await db.commit()
    cleanup.append(delete(Resource).where(Resource.resource_id_bytes == resource.resource_id_bytes))
    assert resource.resource_id is not None
