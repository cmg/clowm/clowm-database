import pytest
from sqlalchemy import delete
from sqlalchemy.ext.asyncio import AsyncSession

from clowmdb.models import User, Workflow
from clowmdb.tests.testutils import CleanupList, random_lower_string


@pytest.mark.asyncio
async def test_workflow_insert(db: AsyncSession, random_user: User, cleanup: CleanupList) -> None:
    """
    Test for inserting a workflow successfully into the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    random_user : clowmdb.models.User
        Random user in database.
    cleanup : list[sqlalchemy.sql.Executable]
        List where to add sql statements that get executed after the (failed) test.
    """
    workflow = Workflow(
        name=random_lower_string(),
        repository_url=random_lower_string(),
        short_description=random_lower_string(),
        developer_id_bytes=random_user.uid.bytes,
    )
    db.add(workflow)
    await db.commit()
    cleanup.append(delete(Workflow).where(Workflow.workflow_id_bytes == workflow.workflow_id_bytes))
    assert workflow.developer_id == random_user.uid
    assert workflow.workflow_id is not None
