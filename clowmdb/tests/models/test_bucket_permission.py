import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from clowmdb.models import Bucket, BucketPermission, User


@pytest.mark.asyncio
async def test_bucket_permission_insert(db: AsyncSession, random_user: User, random_bucket: Bucket) -> None:
    """
    Test for inserting a bucket permission successfully into the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    random_user : clowmdb.models.User
        Random user in database.
    random_bucket : clowmdb.models.Bucket
        Random bucket in database.
    """
    permission = BucketPermission(uid_bytes=random_user.uid.bytes, bucket_name=random_bucket.name)
    db.add(permission)
    await db.commit()
    await db.refresh(permission)
    assert permission.uid == random_user.uid
    assert permission.bucket_name == random_bucket.name
