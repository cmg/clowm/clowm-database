import pytest
from sqlalchemy import insert
from sqlalchemy.ext.asyncio import AsyncSession

from clowmdb.models import Role, User, UserRoleMapping
from clowmdb.models.role import RoleIdMapping


@pytest.mark.asyncio
async def test_add_role_to_user(db: AsyncSession, random_user: User) -> None:
    """
    Test for assigning a user a role successfully in the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    random_user : clowmdb.models.User
        Random usr for testing
    """
    roleMap = RoleIdMapping()
    await db.execute(
        insert(UserRoleMapping).values(uid_bytes=random_user.uid_bytes, role_id_bytes=roleMap[Role.RoleEnum.USER].bytes)
    )
    await db.commit()
    await db.refresh(random_user, attribute_names=["roles"])
    assert len(random_user.roles) == 1
    assert random_user.roles[0].role_id == roleMap[Role.RoleEnum.USER]
