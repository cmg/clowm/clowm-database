import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from clowmdb.models import User, WorkflowExecution, WorkflowVersion


@pytest.mark.asyncio
async def test_workflow_execution_insert(
    db: AsyncSession, random_workflow_version: WorkflowVersion, random_user: User
) -> None:
    """
    Test for inserting a workflow execution successfully into the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    random_workflow_version : clowmdb.models.WorkflowVersion
        Random workflow version in database.
    random_user : clowmdb.models.User
        Random user in database.
    """
    execution = WorkflowExecution(
        executor_id_bytes=random_user.uid.bytes,
        workflow_version_id=random_workflow_version.git_commit_hash,
        slurm_job_id=-1,
    )
    db.add(execution)
    await db.commit()
    await db.refresh(execution)
    assert execution.workflow_version_id == random_workflow_version.git_commit_hash
    assert execution.executor_id == random_user.uid
    assert execution.execution_id is not None
    await db.delete(execution)
    await db.commit()
