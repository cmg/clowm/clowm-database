import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from clowmdb.models import Resource, ResourceVersion
from clowmdb.tests.testutils import random_lower_string


@pytest.mark.asyncio
async def test_resource_version_insert(db: AsyncSession, random_resource: Resource) -> None:
    """
    Test for inserting a resource version successfully into the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    random_resource : clowmdb.models.Resource
        Random resource in database.
    """
    resource_version = ResourceVersion(
        release=random_lower_string(16),
        resource_id_bytes=random_resource.resource_id.bytes,
        synchronization_request_uid_bytes=random_resource.maintainer_id_bytes,
    )
    db.add(resource_version)
    await db.commit()
    await db.commit()
    await db.refresh(resource_version)
    assert resource_version.resource_version_id is not None


@pytest.mark.asyncio
async def test_resource_version_insert_with_previous_version(
    db: AsyncSession, random_resource: Resource, random_resource_version: ResourceVersion
) -> None:
    """
    Test for inserting a resource version with a previous version successfully into the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    random_resource : clowmdb.models.Resource
        Random resource in database.
    random_resource_version : clowmdb.models.ResourceVersion
        Random resource version in database.
    """
    resource_version = ResourceVersion(
        release=random_lower_string(16),
        resource_id_bytes=random_resource.resource_id.bytes,
        previous_resource_version_id_bytes=random_resource_version.resource_version_id.bytes,
    )
    db.add(resource_version)
    await db.commit()
    await db.commit()
    await db.refresh(resource_version)
    assert resource_version.resource_version_id is not None
    assert resource_version.previous_resource_version_id == random_resource_version.resource_version_id
