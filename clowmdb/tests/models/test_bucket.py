import pytest
from sqlalchemy import delete
from sqlalchemy.ext.asyncio import AsyncSession

from clowmdb.models import Bucket, User
from clowmdb.tests.testutils import CleanupList, random_lower_string


@pytest.mark.asyncio
async def test_bucket_insert(db: AsyncSession, random_user: User, cleanup: CleanupList) -> None:
    """
    Test for inserting a bucket successfully into the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    random_user : clowmdb.models.User
        Random user in database.
    cleanup : list[sqlalchemy.sql.Executable]
        List where to add sql statements that get executed after the (failed) test.
    """
    bucket = Bucket(name=random_lower_string(), description=random_lower_string(), owner_id_bytes=random_user.uid.bytes)
    db.add(bucket)
    await db.commit()
    cleanup.append(delete(Bucket).where(Bucket.name == bucket.name))
    assert bucket.name is not None
