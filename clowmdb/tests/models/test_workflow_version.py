import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from clowmdb.models import Workflow, WorkflowMode, WorkflowVersion
from clowmdb.tests.testutils import random_hex_string, random_lower_string


@pytest.mark.asyncio
async def test_workflow_version_insert(db: AsyncSession, random_workflow: Workflow) -> None:
    """
    Test for inserting a workflow version without a previous version successfully into the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    random_workflow : clowmdb.models.Workflow
        Random workflow in database.
    """
    version = WorkflowVersion(
        git_commit_hash=random_hex_string(),
        version=random_lower_string(5),
        workflow_id_bytes=random_workflow.workflow_id.bytes,
    )
    db.add(version)
    await db.commit()
    await db.refresh(version)
    assert version.workflow_id == random_workflow.workflow_id
    assert version.git_commit_hash is not None


@pytest.mark.asyncio
async def test_workflow_version_insert_with_previous_version(
    db: AsyncSession, random_workflow: Workflow, random_workflow_version: WorkflowVersion
) -> None:
    """
    Test for inserting a workflow version with a previous version successfully into the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    random_workflow : clowmdb.models.Workflow
        Random workflow in database.
    random_workflow_version : clowmdb.models.WorkflowVersion
        Random workflow version in database.
    """
    version = WorkflowVersion(
        git_commit_hash=random_hex_string(),
        version=random_lower_string(5),
        workflow_id_bytes=random_workflow.workflow_id.bytes,
        previous_version_hash=random_workflow_version.git_commit_hash,
    )
    db.add(version)
    await db.commit()
    await db.refresh(version)
    assert version.workflow_id == random_workflow.workflow_id
    assert version.previous_version_hash == random_workflow_version.git_commit_hash
    assert version.git_commit_hash is not None


@pytest.mark.asyncio
async def test_workflow_version_delete_with_mode(
    db: AsyncSession, random_workflow_version: WorkflowVersion, random_workflow_mode: WorkflowMode
) -> None:
    """
    Test for inserting a workflow version with a previous version successfully into the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    random_workflow_mode : clowmdb.models.WorkflowMode
        Random workflow mode in database.
    random_workflow_version : clowmdb.models.WorkflowVersion
        Random workflow version in database.
    """
    await db.delete(random_workflow_version)
    await db.commit()
    assert random_workflow_mode is not None
