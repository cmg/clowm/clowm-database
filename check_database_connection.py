import logging

from sqlalchemy import text
from tenacity import after_log, before_log, retry, stop_after_attempt, wait_fixed

from clowmdb.db.session import get_session
from envutil import get_url

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

max_tries = 60 * 3  # 3 minutes
wait_seconds = 2


@retry(
    stop=stop_after_attempt(max_tries),
    wait=wait_fixed(wait_seconds),
    before=before_log(logger, logging.INFO),
    after=after_log(logger, logging.WARN),
)
def init(db_url: str) -> None:
    try:
        with get_session(url=db_url) as db:
            # Try to create session to check if DB is awake
            db.connection().execute(text("SELECT 1"))
    except Exception as e:
        logger.error(e)
        raise e


def main() -> None:
    logger.info("Initializing DB")
    url = get_url(async_url=False)
    init(url)
    logger.info("DB finished initializing")


if __name__ == "__main__":
    main()
