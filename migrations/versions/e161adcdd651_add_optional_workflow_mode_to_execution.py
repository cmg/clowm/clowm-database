"""Add optional workflow mode to execution

Revision ID: e161adcdd651
Revises: 61070219989b
Create Date: 2023-08-23 12:16:18.604560

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "e161adcdd651"
down_revision = "61070219989b"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column("workflow_execution", sa.Column("workflow_mode_id", sa.BINARY(length=16), nullable=True))
    op.create_foreign_key(
        "workflow_execution_with_mode",
        "workflow_execution",
        "workflow_mode",
        ["workflow_mode_id"],
        ["mode_id"],
        ondelete="SET NULL",
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint("workflow_execution_with_mode", "workflow_execution", type_="foreignkey")
    op.drop_column("workflow_execution", "workflow_mode_id")
    # ### end Alembic commands ###
