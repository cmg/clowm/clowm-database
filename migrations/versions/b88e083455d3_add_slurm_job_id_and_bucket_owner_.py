"""Add slurm_job_id and bucket owner_constrain

Revision ID: b88e083455d3
Revises: 18aee1a5d170
Create Date: 2023-02-17 19:44:16.846992

"""

import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = "b88e083455d3"
down_revision = "18aee1a5d170"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column("bucket", sa.Column("owner_constraint", mysql.ENUM("READ", "WRITE"), nullable=True))
    op.add_column("workflow_execution", sa.Column("slurm_job_id", sa.Integer(), nullable=False))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("workflow_execution", "slurm_job_id")
    op.drop_column("bucket", "owner_constraint")
    # ### end Alembic commands ###
