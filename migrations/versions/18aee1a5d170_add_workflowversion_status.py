"""Add Workflowversion status

Revision ID: 18aee1a5d170
Revises: 47d1ffbbfc92
Create Date: 2023-02-15 11:40:03.086523

"""

import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = "18aee1a5d170"
down_revision = "47d1ffbbfc92"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "workflow_version",
        sa.Column("status", mysql.ENUM("CREATED", "DENIED", "PUBLISHED", "DEPRECATED"), nullable=False),
    )
    op.drop_column("workflow_version", "published")
    op.drop_column("workflow_version", "deprecated")
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "workflow_version",
        sa.Column(
            "deprecated",
            mysql.TINYINT(display_width=1),
            server_default=sa.text("'0'"),
            autoincrement=False,
            nullable=False,
        ),
    )
    op.add_column(
        "workflow_version",
        sa.Column(
            "published",
            mysql.TINYINT(display_width=1),
            server_default=sa.text("'0'"),
            autoincrement=False,
            nullable=False,
        ),
    )
    op.drop_column("workflow_version", "status")
    # ### end Alembic commands ###
