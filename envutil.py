from os import environ as env
from typing import Optional

from dotenv import load_dotenv

load_dotenv()


def get_env_variable(name: str, default: Optional[str] = None) -> str:
    if name in env.keys():
        return env[name]
    variable_file = name + "_FILE"
    if variable_file in env.keys():
        with open(env[variable_file]) as f:
            return f.read()
    if default is None:
        raise KeyError(f"Variable with name {name} does not exist.")
    return default


def get_url(async_url: bool = True) -> str:
    return "mysql+{}://{}:{}@{}:{}/{}".format(
        "aiomysql" if async_url else "pymysql",
        get_env_variable("DB_USER"),
        get_env_variable("DB_PASSWORD"),
        get_env_variable("DB_HOST"),
        get_env_variable("DB_PORT", default="3306"),
        get_env_variable("DB_DATABASE"),
    )
